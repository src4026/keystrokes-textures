# Minetest Keystrokes Textures

This repository is a collection of textures for the [Keystrokes CSM](https://github.com/Minetest-j45/keystrokes) for [Minetest](https://github.com/minetest/minetest) based on the [Catppuccin Mocha](https://github.com/catppuccin/catppuccin) palette. These textures are also bundled along with my [Cosmic Tranquility texture pack](https://gitlab.com/src4026/cosmic_tranquility).

## License
This project is licensed under [CC BY-SA 4.0](LICENSE).
